const app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
let bodyParser = require('body-parser');
let config = require('./config');

const PORT = config.server.port;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

app.use((req, res, next) => {
    console.log(`URL: ${req.originalUrl} BODY: ${JSON.stringify(req.body)}`);
    next();
});

app.get('/lesson/:id', (req, res) => {
    res.render('lesson.ejs', { id: req.params.id });
});

app.get('/interview/:id', (req, res) => {
    res.render('interview.ejs', { id: req.params.id });
});


http.listen(PORT, () => console.log(`Server has started on ${PORT}`));